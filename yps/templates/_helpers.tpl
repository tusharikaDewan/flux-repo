{{- define "ddTomcatConfig" }}
{{- printf " [{\"is_jmx\": \"true\",\"collect_default_metrics\": \"true\",\"conf\": [{\"include\": {\"type\": \"ThreadPool\",\"attribute\": {\"maxThreads\": {\"alias\": \"tomcat.threads.max\",\"metric_type\": \"gauge\"},\"currentThreadCount\": {\"alias\": \"tomcat.threads.count\",\"metric_type\": \"gauge\"},\"currentThreadsBusy\": {\"alias\": \"tomcat.threads.busy\",\"metric_type\": \"gauge\"}}}},{\"include\": {\"type\": \"GlobalRequestProcessor\",\"attribute\": {\"bytesSent\": {\"alias\": \"tomcat.bytes_sent\",\"metric_type\": \"counter\"},\"bytesReceived\": {\"alias\": \"tomcat.bytes_rcvd\",\"metric_type\": \"counter\"},\"errorCount\": {\"alias\": \"tomcat.error_count\",\"metric_type\":\"counter\"},\"requestCount\": {\"alias\": \"tomcat.request_count\",\"metric_type\": \"counter\"},\"maxTime\": {\"alias\": \"tomcat.max_time\",\"metric_type\": \"gauge\"},\"processingTime\": {\"alias\": \"tomcat.processing_time\",\"metric_type\": \"counter\"}}}},{\"include\": {\"j2eeType\": \"Servlet\",\"attribute\": {\"processingTime\": {\"alias\": \"tomcat.servlet.processing_time\",\"metric_type\": \"counter\"},\"errorCount\": {\"alias\": \"tomcat.servlet.error_count\",\"metric_type\": \"counter\"},\"requestCount\": {\"alias\": \"tomcat.servlet.request_count\",\"metric_type\": \"counter\"}}}},{\"include\": {\"type\": \"Cache\",\"attribute\": {\"accessCount\": {\"alias\": \"tomcat.cache.access_count\",\"metric_type\": \"counter\"},\"hitsCounts\": {\"alias\": \"tomcat.cache.hits_count\",\"metric_type\": \"counter\"}}}},{\"include\": {\"type\": \"JspMonitor\",\"attribute\": {\"jspCount\": {\"alias\": \"tomcat.jsp.count\",\"metric_type\": \"counter\"},\"jspReloadCount\": {\"alias\": \"tomcat.jsp.reload_count\",\"metric_type\": \"counter\"}}}}]}]" }}
{{- end }}


{{- define "imagePullSecret" }}
{{- with .Values.image }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .repository .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}


{{- define "flywayImagePullSecret" }}
{{- with .Values.flywayImageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}


{{- define "flyway_location" }}
{{- if .Values.RUN_YPS_CONTRACT }}
{{- "filesystem:/flyway/migrations,filesystem:/flyway/contract" }}
{{- else }}
{{- "filesystem:/flyway/migrations" }}
{{- end }}
{{- end }}

