
-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.27-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Table structure for table `dbversion`
--

DROP TABLE IF EXISTS `dbversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbversion` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Version` varchar(10) NOT NULL DEFAULT '',
  `CreationTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ModificationTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbversion`
--

LOCK TABLES `dbversion` WRITE;
/*!40000 ALTER TABLE `dbversion` DISABLE KEYS */;

INSERT INTO `dbversion` VALUES (1,'5.1.118',UTC_TIMESTAMP(),UTC_TIMESTAMP());
/*!40000 ALTER TABLE `dbversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yps`.`node`
--

DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `nodeID` int(10) unsigned NOT NULL auto_increment,
  `nodeTypeID` int(10) unsigned NOT NULL default '0',
  `tokenId` varchar(64) NOT NULL default '',
  `ttl` int(10) unsigned NOT NULL default '0',
  `isRegistered` tinyint(1) NOT NULL default '0',
  `node` varchar(64) NOT NULL default '0',
  `timeStamp` bigint(20) unsigned NOT NULL default '0',
  `modificaationTime` bigint(20) unsigned NOT NULL default '0',
  `nodeDescription` varchar(256) NOT NULL default '',
  `registrationIdentifierID` int(10) unsigned NOT NULL default '0',
  `ip` varchar(128) DEFAULT NULL,
  PRIMARY KEY  (`nodeID`),
  KEY `FK_node_1` (`nodeTypeID`),
  KEY `FK_node_2` (`registrationIdentifierID`),
  INDEX `Idx_node_tokenId` (`tokenId`),
  CONSTRAINT `FK_node_reg_id` FOREIGN KEY (`registrationIdentifierID`) REFERENCES `registrationidentifier` (`registrationIdentifierID`),
  CONSTRAINT `FK_node_typeId` FOREIGN KEY (`nodeTypeID`) REFERENCES `nodetype` (`nodeTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains information of already registered nodes in YPS.';

--
-- Dumping data for table `yps`.`node`
--

/*!40000 ALTER TABLE `node` DISABLE KEYS */;
/*!40000 ALTER TABLE `node` ENABLE KEYS */;


--
-- Table structure for table `yps`.`nodeproperties`
--

DROP TABLE IF EXISTS `nodeproperties`;
CREATE TABLE `nodeproperties` (
  `nodeID` int(10) unsigned NOT NULL default '0',
  `columnName` varchar(48) NOT NULL default '',
  `columnInfo` MEDIUMTEXT NOT NULL default '',
  PRIMARY KEY  (`nodeID`,`columnName`),
  CONSTRAINT `FK_nodeId` FOREIGN KEY (`nodeID`) REFERENCES `node` (`nodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table containing properties w.r.t. nodes registered';

--
-- Dumping data for table `yps`.`nodeproperties`
--

/*!40000 ALTER TABLE `nodeproperties` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodeproperties` ENABLE KEYS */;


--
-- Table structure for table `yps`.`noderolemapping`
--

DROP TABLE IF EXISTS `noderolemapping`;
CREATE TABLE `noderolemapping` (
  `nodeTypeID` int(10) unsigned NOT NULL default '0',
  `permittedNodesToReceive` int(10) unsigned NOT NULL default '0',
  `permittedNodesToRegister` int(10) unsigned NOT NULL default '0',
  `permittedNodesToDelete` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`nodeTypeID`),
  CONSTRAINT `FK_nodeTypeId` FOREIGN KEY (`nodeTypeID`) REFERENCES `nodetype` (`nodeTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains permission/role mapping of all node types.';

--
-- Dumping data for table `yps`.`noderolemapping`
--

/*!40000 ALTER TABLE `noderolemapping` DISABLE KEYS */;
INSERT INTO `noderolemapping` (`nodeTypeID`,`permittedNodesToReceive`,`permittedNodesToRegister`,`permittedNodesToDelete`) VALUES 
 (1,18,0,0),
 (2,4,2,0),
 (4,511,511,511),
 (8,4,8,0),
 (16,8,16,0),
 (32,4,32,0),
 (64, 4, 64, 0),
 (128, 4, 128, 0),
 (256, 4, 256, 0);
/*!40000 ALTER TABLE `noderolemapping` ENABLE KEYS */;


--
-- Table structure for table `yps`.`nodetype`
--

DROP TABLE IF EXISTS `nodetype`;
CREATE TABLE `nodetype` (
  `nodeTypeID` int(10) unsigned NOT NULL default '0',
  `nodeType` varchar(45) NOT NULL default '',
  `nodeTypeDescription` VARCHAR(50) NOT NULL DEFAULT '',  
  PRIMARY KEY  (`nodeTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yps`.`nodetype`
--

/*!40000 ALTER TABLE `nodetype` DISABLE KEYS */;
INSERT INTO `nodetype` (`nodeTypeID`,`nodeType`,`nodeTypeDescription`) VALUES 
 (1,'1','Client Node'),
 (2,'2','Server Node'),
 (4,'4','Admin Node'),
 (8,'8','Provisioning System Node'),
 (16,'16','EMS Node'),
 (32,'32','Kitchen Node'),
 (64, '64', 'Licgen-RMS Node'),
 (128, '128', 'Device Node'),
 (256, '256', 'Licgen-FIT Node');
/*!40000 ALTER TABLE `nodetype` ENABLE KEYS */;


--
-- Table structure for table `yps`.`registrationidentifier`
--

DROP TABLE IF EXISTS `registrationidentifier`;
CREATE TABLE `registrationidentifier` (
  `registrationIdentifierID` int(10) unsigned NOT NULL auto_increment,
  `UUID` varchar(64) NOT NULL default '',
  `description` varchar(256) NOT NULL default '',
  `nodeTypeID` INT(10) UNSIGNED NOT NULL DEFAULT '1',  
  `timestamp` bigint(20) unsigned NOT NULL default '0',
  `modificationTime` bigint(20) unsigned NOT NULL default '0',
  `status` varchar(8) NOT NULL default '',
  `secretKeyId` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `metadata` TEXT DEFAULT NULL,
  PRIMARY KEY  (`registrationIdentifierID`),
  INDEX `FK_reg_nodTypId` (`nodeTypeID`),
  CONSTRAINT `FK_reg_nodTypId` FOREIGN KEY (`nodeTypeID`) REFERENCES `nodetype` (`nodeTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table containing registered vendors.';

--
-- Dumping data for table `yps`.`registrationidentifier`
--

/*!40000 ALTER TABLE `registrationidentifier` DISABLE KEYS */;
/*!40000 ALTER TABLE `registrationidentifier` ENABLE KEYS */;


--
-- Table structure for table `yps`.`supportednodemap`
--

DROP TABLE IF EXISTS `supportednodemap`;
CREATE TABLE `supportednodemap` (
  `nodeID` int(10) unsigned NOT NULL default '0',
  `vendorID` int(10) unsigned NOT NULL default '0',
  `timestamp` bigint(20) unsigned NOT NULL default '0',
  `modificationTime` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`nodeID`,`vendorID`),
  KEY `FK_supportednodemap_1` (`vendorID`),
  CONSTRAINT `FK_supportednodemap_1` FOREIGN KEY (`vendorID`) REFERENCES `registrationidentifier` (`registrationIdentifierID`),
  CONSTRAINT `FK_supportednodemap_2` FOREIGN KEY (`nodeID`) REFERENCES `node` (`nodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table containing supported nodes w.r.t. some vendor';

--
-- Dumping data for table `yps`.`supportednodemap`
--

/*!40000 ALTER TABLE `supportednodemap` DISABLE KEYS */;
/*!40000 ALTER TABLE `supportednodemap` ENABLE KEYS */;

DROP TABLE IF EXISTS `ttlmgmt`;
CREATE TABLE `ttlmgmt` (
  `nodetype` int(10) unsigned NOT NULL DEFAULT '0',
  `ttlvalue` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`nodetype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains time in seconds for node type supported by YPS.';

--
-- Dumping data for table `yps`.`ttlmgmt`
--

/*!40000 ALTER TABLE `ttlmgmt` DISABLE KEYS */;
INSERT INTO `ttlmgmt` (`nodetype`,`ttlvalue`) VALUES 
 (1,843600),
 (2,3600),
 (4,3600),
 (8,300),
 (16,3600),
 (32,3600),
 (64, 3600),
 (128, 3600),
 (256, 3600);


 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

