#!/bin/bash

usage() {                                     
  echo "Usage: $0 [ -b build ] [ -s Service ]"  1>&2 
}

BRed='\e[1;31m';
RCol='\e[0m';

while getopts "b:s:" option; do
  case "${option}" in
    b) 
      BUILD=${OPTARG}
      ;;
    s)
      SERVICE=${OPTARG}
      ;;  
    \?) 
      echo -e "\n${BRed} >>  Unsupported option: -$OPTARG << ${RCol}\n" >&2
      ;;
  esac
done


if [[ -z "$BUILD" ]] || [[ -z "$SERVICE" ]] ; then
    echo -e "\n${BRed} >>  missing parameter(s) << ${RCol}" >&2;
    usage;
    exit 1

fi

mkdir -p charts
VERSION=${BUILD/-/.} # For following standards of semve
cd charts

helm package ../../$SERVICE --app-version=$BUILD --version=5.3.9002

#helm push --force $SERVICE-$VERSION.tgz local-dev

#helm repo update
